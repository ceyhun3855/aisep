using AisEp.Database;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.DataProtection;
using Microsoft.AspNetCore.HttpOverrides;
using Microsoft.EntityFrameworkCore;
using Microsoft.OpenApi.Models;

namespace AisEp.API;

public class Startup
{
    private readonly IWebHostEnvironment _env;

    public Startup(IConfiguration configuration, IWebHostEnvironment env)
    {
        var builder = new ConfigurationBuilder()
          .SetBasePath(env.ContentRootPath)
          .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
          .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true);

        Configuration = builder.Build();
        _env = env;
    }

    public IConfiguration Configuration { get; }

    public void ConfigureServices(IServiceCollection services)
    {
        var connection = Configuration.GetConnectionString("PostgresqlConnection");
        services.AddDbContext<AisContext>(options => options.UseNpgsql(connection));

        services.AddCors();
        services.AddControllers();

        services.AddSwaggerGen(c =>
        {
            c.SwaggerDoc("v1",
                new OpenApiInfo
                {
                    Title = "Aes API",
                    Version = "v1",
                    TermsOfService = null
                });

            c.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme
            {
                In = ParameterLocation.Header,
                Description = "Please insert JWT with Bearer into field",
                Name = "Authorization",
                Type = SecuritySchemeType.ApiKey
            });
            c.AddSecurityRequirement(new OpenApiSecurityRequirement
            {
                {
                    new OpenApiSecurityScheme
                    {
                        Reference = new OpenApiReference
                        {
                            Type = ReferenceType.SecurityScheme,
                            Id = "Bearer"
                        }
                    },
                    new string[] { }
                }
            });
        });

        services.AddAuthentication(CookieAuthenticationDefaults.AuthenticationScheme)
            .AddCookie(options =>
            {
                options.LoginPath = new PathString("/Login/Index");
                options.AccessDeniedPath = new PathString("/Login/Login");
                options.ExpireTimeSpan = new TimeSpan(14, 0, 0);
            });

        services.AddMvc();
        services.ConfigureApplicationCookie(options =>
        {
            options.Cookie.SameSite = SameSiteMode.None;
            options.LoginPath = "/";
            options.ExpireTimeSpan = TimeSpan.FromDays(365);
            options.Cookie.IsEssential = true;
        });

        services.Configure<ForwardedHeadersOptions>(options =>
        {
            options.ForwardedHeaders = ForwardedHeaders.XForwardedFor | ForwardedHeaders.XForwardedProto;
            options.KnownNetworks.Clear();
            options.KnownProxies.Clear();
        });

        services.AddDataProtection()
            .SetApplicationName("aesEp")
            .PersistKeysToFileSystem(new DirectoryInfo(@"/app/temp-keys/"));
    }

    public void Configure(IApplicationBuilder app, IWebHostEnvironment env, AisContext databaseContext)
    {
#if DEBUG
        databaseContext.Database.EnsureCreated();
#endif
        if (env.IsDevelopment()) app.UseDeveloperExceptionPage();
        AppContext.SetSwitch("Npgsql.EnableLegacyTimestampBehavior", true);

        databaseContext.Database.Migrate();
        app.UseSwagger(c => { c.RouteTemplate = "api/swagger/{documentname}/swagger.json"; });

        app.UseSwaggerUI(c =>
        {
            c.SwaggerEndpoint("/api/swagger/v1/swagger.json", "JWT API");
            c.RoutePrefix = "api/swagger";
        });
        app.UseRouting();
        app.UseHttpsRedirection();
        // global cors policy
        app.UseCors(x => x
            .SetIsOriginAllowed(_ => true)
            .AllowAnyMethod()
            .AllowAnyHeader()
            .AllowCredentials());

        app.UseAuthentication();
        app.UseAuthorization();

        app.UseEndpoints(endpoints =>
        {
            endpoints.MapControllerRoute(
                "default",
                "{controller=Search}/{action=Index}/{id?}");
        });
    }
}