﻿using System.Reflection;
using System.Runtime.Loader;
using Microsoft.Extensions.DependencyModel;

namespace AisEp.API.Helpers;

public static class TypeHelper
{
    public static List<Type> getTypesDalProject(string LowwerCaseProjectName)
    {
        var libs = DependencyContext.Default.RuntimeLibraries.Where(x => x.Type == "project"
                                                                         && (x.Name.ToLower().Contains("dal")
                                                                             || x.Name.ToLower()
                                                                                 .Contains(LowwerCaseProjectName)))
            .ToList();

        var types = new List<Type>();
        libs.ForEach(x =>
        {
            var assembly = Assembly.Load(new AssemblyName(x.Name));
            types.AddRange(assembly.GetTypes());
        });
        return types;
    }

    public static List<Type> getTypesDalCoreProject(string LowwerCaseProjectName)
    {
        var libs = DependencyContext.Default.RuntimeLibraries.Where(x => x.Type == "project"
                                                                         && (x.Name.ToLower().Contains("core")
                                                                             || x.Name.ToLower().Contains("dal")
                                                                             || x.Name.ToLower()
                                                                                 .Contains(LowwerCaseProjectName)))
            .ToList();

        var types = new List<Type>();
        libs.ForEach(x =>
        {
            var assembly = Assembly.Load(new AssemblyName(x.Name));
            types.AddRange(assembly.GetTypes());
        });
        return types;
    }

    public static List<Type> getTypesDalApiCoreProject(string LowwerCaseProjectName)
    {
        var libs = DependencyContext.Default.RuntimeLibraries.Where(x => x.Type == "project"
                                                                         && (x.Name.ToLower().Contains("core")
                                                                             || x.Name.ToLower().Contains("dal")
                                                                             || x.Name.ToLower()
                                                                                 .Contains(LowwerCaseProjectName)))
            .ToList();

        var types = new List<Type>();
        libs.ForEach(x =>
        {
            var assembly = Assembly.Load(new AssemblyName(x.Name));
            types.AddRange(assembly.GetTypes());
        });
        return types;
    }

    public static List<Type> GetServiceAndRepositoryTypes(List<Type> types)
    {
        return types
            .Where(x => (x.Name.EndsWith("Service") || x.Name.EndsWith("Repository")) && !x.IsInterface).ToList();
    }


    public class CustomAssemblyLoadContext : AssemblyLoadContext
    {
        public IntPtr LoadUnmanagedLibrary(string absolutePath)
        {
            return LoadUnmanagedDll(absolutePath);
        }

        protected override IntPtr LoadUnmanagedDll(string unmanagedDllName)
        {
            return LoadUnmanagedDllFromPath(unmanagedDllName);
        }

        protected override Assembly Load(AssemblyName assemblyName)
        {
            throw new NotImplementedException();
        }
    }
}