﻿using AisEp.Database;
using AisEp.Database.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SphinxConnector.FluentApi;

namespace AisEp.API;

[ApiController]
[Route("api/[controller]")]
[AllowAnonymous]
public class ConsumersController : ControllerBase
{
    private readonly ILogger<ConsumersController> _logger;
    private readonly AisContext _dbContext;

    public ConsumersController(ILogger<ConsumersController> logger, AisContext dbContext)
    {
        _logger = logger;
        _dbContext = dbContext;
    }

    [HttpGet]
    public async Task<IActionResult> List(string query)
    {
        IFulltextStore fulltextStore = new FulltextStore().Initialize();
        using var session = fulltextStore.StartSession();
        var consumerIds = await session.Query<Consumer>()
            .Match(query)
            .Select(x => x.Id)
            .ToListAsync();
        var consumers = await _dbContext.Consumers.Where(x => consumerIds.Any(y => y == x.Id)).ToListAsync();
        return Ok(consumers);
    }
}