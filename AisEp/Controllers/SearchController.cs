﻿using System.Text;
using AisEp.Database;
using AisEp.Database.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;

namespace AisEp.API.Controllers;

public class SearchController : Controller
{
    private readonly AisContext _dbContext;

    public SearchController(AisContext dbContext)
    {
        _dbContext = dbContext;
    }

    [HttpGet]
    public async Task<IActionResult> Index()
    {
        //IFulltextStore fulltextStore = new FulltextStore().Initialize();
        //using var session = fulltextStore.StartSession();
        //var consumerIds = await session.Query<Consumer>()
        //    .Match("ооо")
        //    .Select(x => x.Id)
        //    .ToListAsync();
        //var consumers = await _dbContext.Consumers.Where(x => consumerIds.Any(y => y == x.Id)).ToListAsync();
        var consumers = await _dbContext.Consumers.ToListAsync();

        return View(consumers);
    }

    [HttpGet]
    public async Task<string> CreateTask(int id)
    {
        var httpClient = new HttpClient();
        var consumer = await _dbContext.Consumers.SingleAsync(x => x.Id == id);
        var data = new
        {
            variables = new
            {
                address = new
                {
                    value = consumer.Address,
                    type = "String"
                },
                code = new
                {
                    value = consumer.Code,
                    type = "String"
                },
                email = new
                {
                    value = consumer.Email,
                    type = "String"
                },
                history = new
                {
                    value = consumer.History,
                    type = "String"
                },
                treatyNumber = new
                {
                    value = consumer.TreatyNumber,
                    type = "String"
                },
                name = new
                {
                    value = consumer.Name,
                    type = "String"
                }
            }
        };
        var content = new StringContent(JsonConvert.SerializeObject(data), Encoding.UTF8, "application/json");


        await httpClient.PostAsync(
            "http://94.26.228.215:8080/engine-rest/process-definition/key/Process_0k7ky2y/submit-form", content);

        return "Заявка успешно создана!";
    }
}