﻿using AisEp.Database.Models;
using Microsoft.EntityFrameworkCore;

namespace AisEp.Database;

public partial class AisContext : DbContext
{
    public AisContext()
    {
    }

    public AisContext(DbContextOptions<AisContext> options)
        : base(options)
    {
    }

    public virtual DbSet<Consumer> Consumers { get; set; } = null!;

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
    }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<Consumer>(entity =>
        {
            entity.HasNoKey();

            entity.ToTable("consumers");

            entity.HasIndex(e => e.Id, "consumers_id_uindex")
                .IsUnique();

            entity.Property(e => e.Address).HasColumnName("address");

            entity.Property(e => e.Code).HasColumnName("code");

            entity.Property(e => e.ConsumerType).HasColumnName("consumerType");

            entity.Property(e => e.Email).HasColumnName("email");

            entity.Property(e => e.Id).HasColumnName("id");

            entity.Property(e => e.Name).HasColumnName("name");

            entity.Property(e => e.TreatyNumber).HasColumnName("treatyNumber");
        });

        OnModelCreatingPartial(modelBuilder);
    }

    partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
}