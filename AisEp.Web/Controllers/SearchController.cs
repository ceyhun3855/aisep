﻿using Microsoft.AspNetCore.Mvc;
using AisEp.Database;
using AisEp.Database.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using SphinxConnector.FluentApi;


namespace AisEp.Web.Controllers;

public class SearchController : Controller
{
    private readonly AisContext _dbContext;

    public SearchController(AisContext dbContext)
    {
        _dbContext = dbContext;
    }

    [HttpGet]
    public async Task<IActionResult> Index()
    {
        IFulltextStore fulltextStore = new FulltextStore().Initialize();
        using var session = fulltextStore.StartSession();
        var consumerIds = await session.Query<Consumer>()
            .Match("ооо")
            .Select(x => x.Id)
            .ToListAsync();
        var consumers = await _dbContext.Consumers.Where(x => consumerIds.Any(y => y == x.Id)).ToListAsync();
        return View(consumers);
    }

    [HttpGet]
    public async Task<IActionResult> List()
    {
        IFulltextStore fulltextStore = new FulltextStore().Initialize();
        using var session = fulltextStore.StartSession();
        var consumerIds = await session.Query<Consumer>()
            .Match("ооо")
            .Select(x => x.Id)
            .ToListAsync();
        var consumers = await _dbContext.Consumers.Where(x => consumerIds.Any(y => y == x.Id)).ToListAsync();
        return View(consumers);
    }
}